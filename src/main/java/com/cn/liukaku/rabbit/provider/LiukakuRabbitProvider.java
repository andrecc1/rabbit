package com.cn.liukaku.rabbit.provider;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class LiukakuRabbitProvider {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(){
        String content="hello"+new Date();

        System.out.println("RabbitProvider" +content);

        amqpTemplate.convertAndSend("HelloRabbit",content);
    }

}
