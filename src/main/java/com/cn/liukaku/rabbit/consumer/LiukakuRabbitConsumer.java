package com.cn.liukaku.rabbit.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "HelloRabbit")
public class LiukakuRabbitConsumer {

    @RabbitHandler
    public void process(String content) {
        System.out.println("Consumer :" + content);
    }
}
