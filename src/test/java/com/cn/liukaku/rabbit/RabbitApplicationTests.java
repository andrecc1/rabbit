package com.cn.liukaku.rabbit;

import com.cn.liukaku.rabbit.provider.LiukakuRabbitProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = RabbitApplication.class)
class RabbitApplicationTests {

    @Autowired
    private LiukakuRabbitProvider liukakuRabbitProvider;

    @Test
    void contextLoads() {
        for (int i = 0; i <100 ; i++) {
            liukakuRabbitProvider.send();
        }
    }
}
